minetest.chat_send_all("Getting Hands Dirty Mod loaded!")

getting_hands_dirty = {}
local finitewater = minetest.setting_getbool("liquid_finite")

dofile(minetest.get_modpath("getting_hands_dirty") .. "/functions.lua")

--
-- nodes
--
dofile(minetest.get_modpath("getting_hands_dirty") .. "/nodes/filthy_source.lua")
dofile(minetest.get_modpath("getting_hands_dirty") .. "/nodes/filthy_flowing.lua")
dofile(minetest.get_modpath("getting_hands_dirty") .. "/nodes/water_filter.lua")
if mod_loaded("pipeworks") then
  dofile(minetest.get_modpath("getting_hands_dirty") .. "/nodes/filth_pump.lua")
  --dofile(minetest.get_modpath("getting_hands_dirty") .. "/nodes/pipes.lua")
  --dofile(minetest.get_modpath("getting_hands_dirty") .. "/nodes/pipe_3_filthloaded.lua")
end

--
-- overrides
--
dofile(minetest.get_modpath("getting_hands_dirty") .. "/overrides/default_water.lua")
dofile(minetest.get_modpath("getting_hands_dirty") .. "/overrides/default_corals.lua")
if mod_loaded("homedecor") then
  dofile(minetest.get_modpath("getting_hands_dirty") .. "/overrides/homedecor_toilet.lua")
end
