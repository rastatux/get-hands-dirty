local pipes_filthfull_nodenames = {}

getting_hands_dirty.pipe_selectboxes = {
	{ -32/64,  -8/64,  -8/64,  8/64,  8/64,  8/64 },
	{ -8/64 ,  -8/64,  -8/64, 32/64,  8/64,  8/64 },
	{ -8/64 , -32/64,  -8/64,  8/64,  8/64,  8/64 },
	{ -8/64 ,  -8/64,  -8/64,  8/64, 32/64,  8/64 },
	{ -8/64 ,  -8/64, -32/64,  8/64,  8/64,  8/64 },
	{ -8/64 ,  -8/64,  -8/64,  8/64,  8/64, 32/64 }
}

local vti = {4, 3, 2, 1, 6, 5}
local cconnects = {{}, {1}, {1, 2}, {1, 3}, {1, 3, 5}, {1, 2, 3}, {1, 2, 3, 5}, {1, 2, 3, 4}, {1, 2, 3, 4, 5}, {1, 2, 3, 4, 5, 6}}
for index, connects in ipairs(cconnects) do
	local outsel = {}

	local jx = 0
	local jy = 0
	local jz = 0
	for _, v in ipairs(connects) do
		if v == 1 or v == 2 then
			jx = jx + 1
		elseif v == 3 or v == 4 then
			jy = jy + 1
		else
			jz = jz + 1
		end
		table.insert(outsel, getting_hands_dirty.pipe_selectboxes[v])
	end

	if #connects == 1 then
		local v = connects[1]
		v = v-1 + 2*(v%2) -- Opposite side
	end

	local pgroups = {snappy = 3, pipe = 1, not_in_creative_inventory = 1}
	local pipedesc = "Pipe segement".." "..dump(connects).."... You hacker, you."
	local image = nil

	if #connects == 0 then
		pgroups = {snappy = 3, tube = 1}
		pipedesc = "Pipe segment"
		image = "pipeworks_pipe_inv.png"
	end

	local outimg_l = { "pipeworks_pipe_plain.png" }
	if index == 3 then
		outimg_l = { "pipeworks_pipe_3_filthloaded.png" }
	end

	local mesh = "pipeworks_pipe_"..index..".obj"

	if index == 1 then
		mesh = "pipeworks_pipe_3.obj"
	end

	local pgroups = {snappy = 3, pipe = 1, not_in_creative_inventory = 1}

	minetest.register_node("getting_hands_dirty:pipe_"..index.."_filthloaded", {
		description = pipedesc,
		drawtype = "mesh",
		mesh = mesh,
		tiles = outimg_l,
		sunlight_propagates = true,
		paramtype = "light",
		paramtype2 = "facedir",
		selection_box = {
			type = "fixed",
			fixed = outsel
		},
		collision_box = {
			type = "fixed",
			fixed = outsel
		},
		groups = pgroups,
		sounds = default.node_sound_wood_defaults(),
		walkable = true,
		drop = "pipeworks:pipe_1_empty",
		after_place_node = function(pos)
			pipeworks.scan_for_pipe_objects(pos)
		end,
		after_dig_node = function(pos)
			pipeworks.scan_for_pipe_objects(pos)
		end
	})

	table.insert(pipes_filthfull_nodenames,  "pipeworks:pipe_"..index.."_filthloaded")
end

minetest.register_abm({
	nodenames = pipes_empty_nodenames,
	interval = 1,
	chance = 1,
	action = function(pos, node, active_object_count, active_object_count_wider)
    getting_hands_dirty.check_for_filth_inflows(pos,node)
	end
})

minetest.register_abm({
	nodenames = pipes_filthfull_nodenames,
	interval = 1,
	chance = 1,
	action = function(pos, node, active_object_count, active_object_count_wider)
		getting_hands_dirty.check_sources(pos,node)
	end
})

minetest.register_abm({
	nodenames = {"pipeworks:spigot","pipeworks:spigot_pouring"},
	interval = 1,
	chance = 1,
	action = function(pos, node, active_object_count, active_object_count_wider)
		getting_hands_dirty.spigot_check(pos,node)
	end
})

getting_hands_dirty.check_for_liquids = function(pos)
	local coords = {
		{x=pos.x,y=pos.y-1,z=pos.z},
		{x=pos.x,y=pos.y+1,z=pos.z},
		{x=pos.x-1,y=pos.y,z=pos.z},
		{x=pos.x+1,y=pos.y,z=pos.z},
		{x=pos.x,y=pos.y,z=pos.z-1},
		{x=pos.x,y=pos.y,z=pos.z+1},	}
	for i =1,6 do
		local name = minetest.get_node(coords[i]).name
		if name and string.find(name,"water") then
			if finitewater then minetest.remove_node(coords[i]) end
			return true
		end
	end
	return false
end

getting_hands_dirty.check_for_inflows = function(pos,node)
	local coords = {
		{x=pos.x,y=pos.y-1,z=pos.z},
		{x=pos.x,y=pos.y+1,z=pos.z},
		{x=pos.x-1,y=pos.y,z=pos.z},
		{x=pos.x+1,y=pos.y,z=pos.z},
		{x=pos.x,y=pos.y,z=pos.z-1},
		{x=pos.x,y=pos.y,z=pos.z+1},	}
	local newnode = false
	local source = false
	for i =1,6 do
		if newnode then break end
		local name = minetest.get_node(coords[i]).name
		if name and (name == "pipeworks:pump_on" and pipeworks.check_for_liquids(coords[i])) or string.find(name,"_loaded") then
			if string.find(name,"_loaded") then
				source = minetest.get_meta(coords[i]):get_string("source")
				if source == minetest.pos_to_string(pos) then break end
			end
			newnode = string.gsub(node.name,"empty","loaded")
			source = {x=coords[i].x,y=coords[i].y,z=coords[i].z}
		end
	end
	if newnode then
		minetest.add_node(pos,{name=newnode, param2 = node.param2})
		minetest.get_meta(pos):set_string("source",minetest.pos_to_string(source))
	end
end

getting_hands_dirty.check_sources = function(pos,node)
	local sourcepos = minetest.string_to_pos(minetest.get_meta(pos):get_string("source"))
	if not sourcepos then return end
	local source = minetest.get_node(sourcepos).name
	local newnode = false
	if source and not ((source == "getting_hands_dirty:filth_pump_on" and getting_hands_dirty.check_for_liquids(sourcepos)) or string.find(source,"_filthloaded") or source == "ignore" ) then
		newnode = string.gsub(node.name,"loaded","empty")
	end

	if newnode then
		minetest.add_node(pos,{name=newnode, param2 = node.param2})
		minetest.get_meta(pos):set_string("source","")
	end
end

getting_hands_dirty.spigot_check = function(pos, node)
	local belowname = minetest.get_node({x=pos.x,y=pos.y-1,z=pos.z}).name
	if belowname and (belowname == "air" or belowname == "getting_hands_dirty:filthy_flowing" or belowname == "getting_hands_dirty:filthy_source") then
		local spigotname = minetest.get_node(pos).name
		local fdir=node.param2 % 4
		local check = {
			{x=pos.x,y=pos.y,z=pos.z+1},
			{x=pos.x+1,y=pos.y,z=pos.z},
			{x=pos.x,y=pos.y,z=pos.z-1},
			{x=pos.x-1,y=pos.y,z=pos.z}
		}
		local near_node = minetest.get_node(check[fdir+1])
		if near_node and string.find(near_node.name, "_loaded") then
			if spigotname and spigotname == "pipeworks:spigot" then
				minetest.add_node(pos,{name = "pipeworks:spigot_pouring", param2 = fdir})
				if finitewater or belowname ~= "getting_hands_dirty:filthy_source" then
					minetest.add_node({x=pos.x,y=pos.y-1,z=pos.z},{name = "getting_hands_dirty:filthy_source"})
				end
			end
		else
			if spigotname == "pipeworks:spigot_pouring" then
				minetest.add_node({x=pos.x,y=pos.y,z=pos.z},{name = "pipeworks:spigot", param2 = fdir})
				if belowname == "getting_hands_dirty:filthy_source" and not finitewater then
					minetest.remove_node({x=pos.x,y=pos.y-1,z=pos.z})
				end
			end
		end
	end
end
