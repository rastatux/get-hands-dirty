minetest.register_node("getting_hands_dirty:filthy_source", {
	description = "Filthy Water Source",
	drawtype = "liquid",
	alpha = 160,
	paramtype = "light",
	walkable     = false,
	pointable    = false,
	diggable     = false,
	buildable_to = false,
	drop	= "",
	drowning = 1,
	liquidtype = "source",
	liquid_alternative_flowing = "getting_hands_dirty:filthy_flowing",
	liquid_alternative_source = "getting_hands_dirty:filthy_source",
	liquid_viscosity = 0.2,
	liquid_range = 6,
	post_effect_color = {a = 103, r = 30, g = 60, b = 90},
	groups = {water = 3, liquid = 3, puts_out_fire = 1, cools_lava = 1},
	--sounds = default.node_sound_water_defaults(),

	inventory_image = minetest.inventorycube("default_filthy_liquid.png"),

	tiles = {
		{
			name = "default_filthy_source_animated.png",
			animation = {
				type     = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length   = 2.0
			}
		}
	},

	special_tiles = {
		-- New-style water source material (mostly unused)
		{
			name      = "default_filthy_source_animated.png",
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 2.0},
			backface_culling = false,
		}
	},

})

minetest.register_abm({
	nodenames = {"getting_hands_dirty:filthy_source"},
	neighbors = {"default:water_source"},
	interval = 5.0,
	chance = 2,
	action = function(pos, node, active_object_count, active_object_count_wider)
    local filthy = "getting_hands_dirty:filthy_source"
    if minetest.get_node({x=pos.x, y=pos.y+1, z=pos.z}).name == filthy then
      minetest.set_node({x = pos.x, y = pos.y, z = pos.z}, {name = filthy})
    elseif minetest.get_node({x=pos.x, y=pos.y-1, z=pos.z}).name == filthy then
      minetest.set_node({x = pos.x, y = pos.y-1, z = pos.z}, {name = "default:water_source"})
    end
    pollute_water(pos, node, filthy)
	end
})

--[[minetest.register_abm({
	nodenames = {"default:water_source"},
	neighbors = {"getting_hands_dirty:filthy_source"},
	interval = 5.0,
	chance = 2,
	action = function(pos, node, active_object_count, active_object_count_wider)
    local filthy = "getting_hands_dirty:filthy_source"
    if minetest.get_node({x=pos.x, y=pos.y+1, z=pos.z}).name == "getting_hands_dirty:filthy_source" then
      minetest.set_node({x = pos.x, y = pos.y, z = pos.z}, {name = "getting_hands_dirty:filthy_source"})
    elseif minetest.get_node({x=pos.x, y=pos.y+1, z=pos.z}).name == "getting_hands_dirty:filthy_flowing" then
      minetest.set_node({x = pos.x, y = pos.y-1, z = pos.z}, {name = "getting_hands_dirty:filthy_source"})
    end
    pollute_water(pos, node, "getting_hands_dirty:filthy_source")
	end
})]]--
