
local pipes_filthfull_nodenames = {}

local cconnects = {{}, {1}, {1, 2}, {1, 3}, {1, 3, 5}, {1, 2, 3}, {1, 2, 3, 5}, {1, 2, 3, 4}, {1, 2, 3, 4, 5}, {1, 2, 3, 4, 5, 6}}

minetest.register_node("getting_hands_dirty:pipe_3_filthloaded", {
  description = "Pipe segment - Loaded with Filthy Water",
  drawtype = "mesh",
  mesh = "pipeworks_pipe_3.obj",
  tiles = { "pipeworks_pipe_3_filthloaded.png" },
  sunlight_propagates = true,
  inventory_image = "pipeworks_pipe_inv.png",
  paramtype = "light",
  paramtype2 = "facedir",
  --[[selection_box = {
    type = "fixed",
    fixed = outsel
  },
  collision_box = {
    type = "fixed",
    fixed = outsel
  },]]--
  groups = {snappy = 3, tube = 1},
  sounds = default.node_sound_wood_defaults(),
  walkable = true,
  drop = "pipeworks:pipe_1_empty",
  after_place_node = function(pos)
    pipeworks.scan_for_pipe_objects(pos)
  end,
  after_dig_node = function(pos)
    pipeworks.scan_for_pipe_objects(pos)
  end
})
