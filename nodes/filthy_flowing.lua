minetest.register_node("getting_hands_dirty:filthy_flowing", {
	description = "Flowing Filthy Water",
	drawtype = "flowingliquid",
  alpha = 160,
	paramtype = "light",
	paramtype2 = "flowingliquid",
	walkable = false,
	pointable = false,
	diggable = false,
	buildable_to = true,
	is_ground_content = false,
	drop = "",
	drowning = 1,
	liquidtype = "flowing",
	liquid_range = 6,
  liquid_alternative_flowing = "getting_hands_dirty:filthy_flowing",
	liquid_alternative_source = "getting_hands_dirty:filthy_source",
	liquid_viscosity = 0.2,
	post_effect_color = {a = 103, r = 30, g = 60, b = 90},
	groups = {water = 3, liquid = 3, puts_out_fire = 1,
		not_in_creative_inventory = 1, cools_lava = 1},
	--sounds = default.node_sound_water_defaults(),

	tiles = {"default_filthy_liquid.png"},
	special_tiles = {
		{
			name = "default_filthy_flowing_animated.png",
			backface_culling = false,
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 0.8,
			},
		},
		{
			name = "default_filthy_flowing_animated.png",
			backface_culling = true,
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 0.8,
			},
		},
	},

})
