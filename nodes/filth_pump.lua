local states = { "on", "off" }
local dgroups = ""

for s in ipairs(states) do

	if states[s] == "off" then
		dgroups = {snappy=3, pipe=1}
	else
		dgroups = {snappy=3, pipe=1, not_in_creative_inventory=1}
	end

	minetest.register_node("getting_hands_dirty:filth_pump_"..states[s], {
		description = "Filth Pump/Intake Module",
		tiles = { "filth_pump.png" },
		paramtype = "light",
		paramtype2 = "facedir",
		groups = dgroups,
		sounds = default.node_sound_wood_defaults(),
		walkable = true,
		after_place_node = function(pos)
			pipeworks.scan_for_pipe_objects(pos)
		end,
		after_dig_node = function(pos)
			pipeworks.scan_for_pipe_objects(pos)
		end,
		drop = "getting_hands_dirty:filth_pump_off"
	})
end

minetest.register_abm({
	nodenames = {"getting_hands_dirty:filth_pump_on"},
	interval = 5.0,
	chance = 1,
	action = function(pos, node, active_object_count, active_object_count_wider)
    minetest.set_node(pos, {name = "getting_hands_dirty:filth_pump_off"})
	end
})
