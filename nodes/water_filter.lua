
minetest.register_node("getting_hands_dirty:water_filter", {
  description = "Water Filter",
  tiles = {
    "water_filter_side.png",
    "water_filter_side.png",
    "water_filter_front.png",
    "water_filter_front.png",
    "water_filter_front.png",
    "water_filter_front.png"
  },
  is_ground_content = true,
  groups = {cracky = 3},
  drop = "getting_hands_dirty:water_filter"
})

minetest.register_abm({
	nodenames = {"getting_hands_dirty:filthy_source"},
	neighbors = {"getting_hands_dirty:water_filter"},
	interval = 5.0,
	chance = 1,
	action = function(pos, node, active_object_count, active_object_count_wider)
		minetest.set_node({x = pos.x, y = pos.y, z = pos.z}, {name = "default:water_source"})
	end
})
