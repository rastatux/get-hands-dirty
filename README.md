# Minetest mod: Getting hands dirty

Includes filthy water and polluted environment to the game.
Still early days, so don't expect more than beta functionality.

**Attention: If filthy water (source or flowing) meets water source, the water gets polluted. Be careful, otherwise whole oceans get polluted.**


## Dependencies

 - default

### Optional
  - pipeworks
  - homedecor
