function pollute_water(pos, node, filthy)
  if minetest.get_node({x=pos.x-1, y=pos.y, z=pos.z}).name == filthy then
    minetest.set_node(pos, {name = filthy})
  elseif minetest.get_node({x=pos.x+1, y=pos.y, z=pos.z}).name == filthy then
    minetest.set_node(pos, {name = filthy})
  elseif minetest.get_node({x=pos.x, y=pos.y, z=pos.z-1}).name == filthy then
    minetest.set_node(pos, {name = filthy})
  elseif minetest.get_node({x=pos.x, y=pos.y, z=pos.z+1}).name == filthy then
    minetest.set_node(pos, {name = filthy})
  end
end

--[[function pollute_water(pos, node, filthy)
  local pollution = 0
  if minetest.get_node({x=pos.x-1, y=pos.y, z=pos.z}).name == filthy then
    pollution = pollution + 1
  end
  if minetest.get_node({x=pos.x+1, y=pos.y, z=pos.z}).name == filthy then
    pollution = pollution + 1
  end
  if minetest.get_node({x=pos.x, y=pos.y, z=pos.z-1}).name == filthy then
    pollution = pollution + 1
  end
  if minetest.get_node({x=pos.x, y=pos.y, z=pos.z+1}).name == filthy then
    pollution = pollution + 1
  end
  if pollution >= 2 then
    minetest.set_node(pos, {name = filthy})
  end
  minetest.chat_send_all("Polution: "..tostring(pollution))
end]]--

minetest.register_abm({
	nodenames = {"default:water_source"},
	neighbors = {"getting_hands_dirty:filthy_source"},
	interval = 10.0,
	chance = 2,
	action = function(pos, node, active_object_count, active_object_count_wider)
    local filthy = "getting_hands_dirty:filthy_source"
    if minetest.get_node({x=pos.x, y=pos.y+1, z=pos.z}).name == filthy then
      minetest.set_node({x = pos.x, y = pos.y, z = pos.z}, {name = filthy})
    elseif minetest.get_node({x=pos.x, y=pos.y-1, z=pos.z}).name == filthy then
      minetest.set_node({x = pos.x, y = pos.y-1, z = pos.z}, {name = "default:water_source"})
    end
    pollute_water(pos, node, filthy)
	end
})

minetest.register_abm({
	nodenames = {"default:water_source"},
	neighbors = {"getting_hands_dirty:filthy_flowing"},
	interval = 5.0,
	chance = 2,
	action = function(pos, node, active_object_count, active_object_count_wider)
    local filthy = "getting_hands_dirty:filthy_source"
    if minetest.get_node({x=pos.x, y=pos.y+1, z=pos.z}).name == "getting_hands_dirty:filthy_flowing" then
      minetest.set_node({x = pos.x, y = pos.y, z = pos.z}, {name = filthy})
    end
    pollute_water(pos, node, filthy)
	end
})
