if mod_loaded("pipeworks")
  then
  minetest.override_item("homedecor:toilet_open", {
    on_punch = function (pos, node, puncher)
      node.name = "homedecor:toilet"
      minetest.set_node(pos, node)
      minetest.sound_play("homedecor_toilet_flush", {
        pos=pos,
        max_hear_distance = 5,
        gain = 1,
      })
      if minetest.get_node_or_nil({x=pos.x, y=pos.y-1, z=pos.z}).name == "getting_hands_dirty:filth_pump_off" or
      minetest.get_node_or_nil({x=pos.x, y=pos.y-1, z=pos.z}).name == "getting_hands_dirty:filth_pump_on" then
        minetest.set_node({x = pos.x, y = pos.y-1, z = pos.z}, {name = "getting_hands_dirty:filth_pump_on"})
      else
        -- TODO If the toilet has no connection to pipes, filthy water would get out on the side.
        --minetest.set_node({x = pos.x+1, y = pos.y, z = pos.z}, {name = "getting_hands_dirty:filthy_source"})
      end
    end
  })
end
