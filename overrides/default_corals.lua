minetest.register_abm({
	nodenames = {"default:coral_brown", "default:coral_orange"},
	neighbors = {"getting_hands_dirty:filthy_source", "getting_hands_dirty:filthy_flowing"},
	interval = 10.0,
	chance = 1,
	action = function(pos, node, active_object_count, active_object_count_wider)
    minetest.set_node({x = pos.x, y = pos.y, z = pos.z}, {name = "default:coral_skeleton"})
	end
})

minetest.register_abm({
	nodenames = {"default:dirt_with_grass"},
	neighbors = {"getting_hands_dirty:filthy_source", "getting_hands_dirty:filthy_flowing"},
	interval = 10.0,
	chance = 1,
	action = function(pos, node, active_object_count, active_object_count_wider)
    minetest.set_node({x = pos.x, y = pos.y, z = pos.z}, {name = "default:dirt_with_dry_grass"})
	end
})
